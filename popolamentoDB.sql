/**** Popolamento della basi di dati ****/


-- Clienti
INSERT INTO Cliente(uid, nome, cognome, dnascita, indirizzo_residenza, citta_residenza)
	VALUES('UID8945', 'Giorgio', 'Campanella', '1971-09-21', 'via del Pendio, 4', 'Pisa');

INSERT INTO Cliente(uid, nome, cognome, dnascita, indirizzo_residenza, citta_residenza)
	VALUES('UID1634', 'Carla', 'Fortunata', '1977-07-17', 'via della iella, 17', 'Padova');

INSERT INTO Cliente(uid, nome, cognome, dnascita, indirizzo_residenza, citta_residenza)
	VALUES('UID4700', 'Elia', 'Da Lontano', '1955-01-3', 'via Pollame, 6', 'Rovigo');

INSERT INTO Cliente(uid, nome, cognome, dnascita, indirizzo_residenza, citta_residenza)
	VALUES('UID0189', 'Francesco', 'Rovello', '1981-12-29', 'via Fontanella, 56', 'Verona');


	
-- Eredi
INSERT INTO Erede(CF, nome, cognome, indirizzo_residenza, citta_residenza)
	VALUES('FRNSMR12E498Q', 'Fortunato', 'Selmore', 'via della Speranza, 1', 'Pisa');

INSERT INTO Erede(CF, nome, cognome, indirizzo_residenza, citta_residenza)
	VALUES('EGNDLVY41F881R', 'Eugenio', 'Dalle Vedove', 'piazza Vittoria, 29', 'Verona');

INSERT INTO Erede(CF, nome, cognome, indirizzo_residenza, citta_residenza)
	VALUES('GVNBNDY41F881R', 'Giovanni', 'Benetti', 'via Palude, 7', 'Verona');


	
-- Agenti
INSERT INTO Agente(matricola, nome, cognome, dnascita, indirizzo_residenza, citta_residenza, tipo_polizza, area, email, password)
	VALUES('AG08145', 'Alessio', 'Brutti', '1972-02-14', 'via Perlar, 9', 'Verona', 'Polizze Immobili', 'Valpolicella', 'alessiobrutti@assicurazionimatera.it', '40773370096b11a30e87e6e2e12d042eacad020a081f78e2ec32bbd171348013');--password = truffaldino72

INSERT INTO Agente(matricola, nome, cognome, dnascita, indirizzo_residenza, citta_residenza, tipo_polizza, area, email, password)
	VALUES('AG09876', 'Gaspare', 'Baffetto', '1982-10-13', 'via Giovani, 39', 'Verona', 'Polizze Vita', 'Polesine', 'gasparebaffetto@assicurazionimatera.it', 'dc20b31b4fc6f2102362f0ccdb90b33f73701527996685745352302112546f1b');/*pischell0*/

INSERT INTO Agente(matricola, nome, cognome, dnascita, indirizzo_residenza, citta_residenza, tipo_polizza, area, email, password)
	VALUES('AG09536', 'Francesca', 'Forlan', '1979-03-19', 'via del Guizzo', 'Napoli', 'Polizze Immobili, Polizze Vita', 'Quartieri Bassi', 'francescaforlan@assicurazionimatera.it', '325e468538c3423221177c95d7263f1360b599e6c8beb1a178694ec09f7635d6');/*s0rtilegi0*/

INSERT INTO Agente(matricola, nome, cognome, dnascita, indirizzo_residenza, citta_residenza, tipo_polizza, area, email, password)
	VALUES('AG11271', 'Domenico', 'Sberla', '1961-11-21', 'via Partenza', 'Napoli', 'Polizze Auto', 'Quartieri Alti', 'domenicosberla@assicurazionimatera.it', '7d30a3dcd00fc6f67ae223a7628737e6d4164c5fa9801cec3942a9f123d536d1');/*schiaffato98*/



-- Qualifiche

INSERT INTO Qualifica(nome, stipendio)
	values('Agente Ausiliario', 900);

INSERT INTO Qualifica(nome, stipendio)
	values('Agente Ordinario di primo livello', 1200);

INSERT INTO Qualifica(nome, stipendio)
	values('Agente Ordinario di secondo livello', 1500);

INSERT INTO Qualifica(nome, stipendio)
	values('Capo Agente', 2000);

	

-- Qualificato
/* Domenico Sberla */
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ausiliario', 'AG11271', '1987-03-06', '1987-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ordinario di primo livello', 'AG11271', '1988-01-01', '1988-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ordinario di secondo livello', 'AG11271', '1989-01-01', '1994-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente,  dinizio)
	VALUES('Capo Agente', 'AG11271', '1995-01-01');

/* Francesca Forlan */
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ausiliario', 'AG09536', '1999-05-21', '2003-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente,  dinizio)
	VALUES('Agente Ordinario di primo livello', 'AG09536', '2004-01-01');

/* Gaspare Baffetto  */
INSERT INTO Qualificato(qualifica, matricola_agente,  dinizio)
	VALUES('Agente Ausiliario', 'AG09876', '2009-09-05');

/* Alessio Brutti */
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ausiliario', 'AG08145', '1992-05-21', '1995-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente, dinizio, dfine)
	VALUES('Agente Ordinario di primo livello', 'AG08145', '1996-01-01', '2006-12-31');
INSERT INTO Qualificato(qualifica, matricola_agente,  dinizio)
	VALUES('Agente Ordinario di secondo livello', 'AG08145', '2007-01-01');



-- Descrizioni
/* auto - vita - immobili*/
INSERT INTO TipoPolizza(tipo, descrizione)
	VALUES('Auto', 'Da anni la nostra assicurazione opera nel settore dei sinistri stradali. Affidati alla nostra esperienza. Conviene!');

INSERT INTO TipoPolizza(tipo, descrizione)
	VALUES('Vita', 'Pensa al futuro dei tuoi cari, affidati ad all azienda leader nel settore delle assicurazioni sulla vita.');

INSERT INTO TipoPolizza(tipo, descrizione)
	VALUES('Immobile', 'Il tuo tetto prima di tutto! La nostra compagnia è la numero uno nella copertura assiurativa di immobili.');



-- Polizze

/* assicurazione auto */
INSERT INTO Polizza(tipo, numero, dinizio, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Auto', '154879', '2011-12-05', 'Bimestrale', '350', 'Sottoscritta', 'UID4700', 'AG09876');
INSERT INTO PolizzaAuto(tipo, numero, targa, massimale, furto_incendio)
	VALUES('Auto', '154879', 'EH548PL', '2000', 'no');

INSERT INTO Polizza(tipo, numero, dinizio, dfine, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Auto', '161984', '2010-01-01', '2011-01-01', 'Annuale', '1400', 'Scaduta', 'UID0189', 'AG11271');
INSERT INTO PolizzaAuto(tipo, numero, targa, massimale, furto_incendio)
	VALUES('Auto', '161984', 'DL156IO', '3000', 'sì');

/* assicurazione vita */
INSERT INTO Polizza(tipo, numero, dinizio, dfine, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Vita', '987845', '2009-03-27', '2012-03-27', 'Annuale', '1500', 'Scaduta', 'UID8945', 'AG09876');
INSERT INTO PolizzaVita(tipo, numero)
	VALUES('Vita', '987845');

INSERT INTO Polizza(tipo, numero, dinizio, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Vita', '359874', '2005-04-02', 'Annuale', '1400', 'Pagata', 'UID0189', 'AG09876');
INSERT INTO PolizzaVita(tipo, numero)
	VALUES('Vita', '359874');

/* assicurazione immobili */
INSERT INTO Polizza(tipo, numero, dinizio, dfine, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Immobile', '845276', '2010-05-01', '2012-05-01', 'Semestrale', '2000', 'Sottoscritta', 'UID4700', 'AG09536');
INSERT INTO PolizzaImmobile(tipo, numero, Indirizzo, Superficie)
	VALUES('Immobile', '845276', 'Via Ferrari, 5', '211');

INSERT INTO Polizza(tipo, numero, dinizio, frequenza_pagamento, importo, stato, uid, matricola_agente)
	VALUES('Immobile', '781456', '2008-11-10', 'Annuale', '2500', 'Sottoscritta', 'UID0189', 'AG09536');
INSERT INTO PolizzaImmobile(tipo, numero, Indirizzo, Superficie)
	VALUES('Immobile', '781456', 'Via Montanara, 71', '292');




INSERT INTO Beneficiario(tipo, numero, CFerede)
	VALUES('Vita','987845','FRNSMR12E498Q');
INSERT INTO Beneficiario(tipo, numero, CFerede)
	VALUES('Vita','987845','EGNDLVY41F881R');
INSERT INTO Beneficiario(tipo, numero, CFerede)
	VALUES('Vita','359874','GVNBNDY41F881R');
INSERT INTO Beneficiario(tipo, numero, CFerede)
	VALUES('Vita','359874','FRNSMR12E498Q');