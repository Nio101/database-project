/**** Creazione della basi di dati ****/

\i cleanDB.sql

CREATE TABLE Cliente(
	uid varchar(10),
	nome varchar(20) NOT NULL,
	cognome varchar(20) NOT NULL,
	dnascita date,
	indirizzo_residenza varchar(40),
	citta_residenza varchar(20),
	PRIMARY KEY (uid)
);

CREATE TABLE TipoPolizza(
	tipo varchar(20),
	descrizione varchar(500),
	PRIMARY KEY(tipo)
);

CREATE TABLE Erede(
	CF varchar(20),
	nome varchar(20),
	cognome varchar(20),
	indirizzo_residenza varchar(40),
	citta_residenza varchar(20),
	PRIMARY KEY(CF)
);

CREATE TABLE Agente	(
	matricola varchar(20),
	nome varchar(20) NOT NULL,
	cognome varchar(20) NOT NULL,
	dnascita date,
	indirizzo_residenza varchar(40),
	citta_residenza varchar(20),
	tipo_polizza varchar(50),
	area varchar(50),
	email varchar(40) NOT NULL,
	password varchar(100) NOT NULL,
	PRIMARY KEY(matricola)
);

CREATE TABLE Qualifica(
	nome varchar(40),
	stipendio integer NOT NULL,
	CHECK (stipendio > 0),
	PRIMARY KEY(nome)
);

CREATE TABLE Polizza(
	tipo varchar(30),
	numero integer,
	dinizio date NOT NULL,
	dfine date,
	frequenza_pagamento varchar(20) DEFAULT 'Annuale',
	importo integer,
	stato varchar(20),
	uid varchar(10) REFERENCES Cliente(uid),
	matricola_agente varchar(20) REFERENCES Agente(matricola),
	CHECK (importo > 0),
	CHECK (dinizio < dfine),
	PRIMARY KEY(tipo, numero)
);

CREATE TABLE PolizzaAuto(
	tipo varchar(10),
	numero integer,
	targa varchar(10) NOT NULL,
	massimale integer NOT NULL,
	furto_incendio varchar(3) NOT NULL,
	CHECK (massimale > 0),
	FOREIGN KEY(tipo, numero) REFERENCES Polizza(tipo, numero),
	PRIMARY KEY(tipo, numero)
);

CREATE TABLE PolizzaVita(
	tipo varchar(10),
	numero integer,
	FOREIGN KEY(tipo, numero) REFERENCES Polizza(tipo, numero),
	PRIMARY KEY(tipo, numero)
);

CREATE TABLE PolizzaImmobile(
	tipo varchar(10),
	numero integer,
	indirizzo varchar(40) NOT NULL,
	superficie integer NOT NULL,
	FOREIGN KEY(tipo, numero) REFERENCES Polizza(tipo, numero),
	PRIMARY KEY(tipo, numero)
);

CREATE TABLE Beneficiario(
	tipo varchar(20),
	numero integer,
	CFerede varchar(20) REFERENCES Erede(CF),
	FOREIGN KEY(tipo, numero) REFERENCES PolizzaVita(tipo, numero),
	PRIMARY KEY(tipo, numero, CFerede)
);

CREATE TABLE Qualificato(
	qualifica varchar(40) REFERENCES Qualifica(nome),
	matricola_agente varchar(20) REFERENCES Agente(matricola),
	dinizio date NOT NULL,
	dfine date,
	CHECK (dinizio < dfine),
	PRIMARY KEY(matricola_agente, qualifica)
);
