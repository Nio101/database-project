/**** Eliminazione delle tabelle ****/

drop table Beneficiario;
drop table Qualificato;
drop table Erede;
drop table PolizzaAuto;
drop table PolizzaImmobile;
drop table PolizzaVita;
drop table Polizza;
drop table Cliente;
drop table TipoPolizza;
drop table Qualifica;
drop table Agente;