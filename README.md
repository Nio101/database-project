# Database project #

This is a university project of the databases course.
It is composed of two parts:


1. database design (ER design and SQL code for PostgreSQL);
2. website design (design with MVC2 pattern and implementation in Tomcat with JSP, Servlets and JAVA).

A better description can be found inside "progetto.pdf" (italian).


# Progetto di basi di dati #
Questo è un progetto universitario del corso di basi di dati.
È composto da due parti:

1. progettazione del database (progetto ER e implementazione SQL in PostgreSQL);
2. progettazione del sito web (progettazione secondo il pattern MVC2 e implementazione in Tomcat con l'utilizzo di JSP, Servlets e JAVA).

Una miglior descrizione del progetto può essere trovata nel file "progetto.pdf".