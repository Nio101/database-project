import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mat.*;


public class main extends HttpServlet {


	/**
	 * Metodo doPost
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
		String ps= "";
		HttpSession session = request.getSession();

		if (request.getParameter("ps") != null) {
			ps= request.getParameter("ps");
		}

		try {
			/**
			 * Login
			 */
			if (ps.equals("login")) {
				DBMS dbms = new DBMS();

				//recupero dei parametri
				String username = request.getParameter("username");
				String password = request.getParameter("password");

				//autenticazione
				if (dbms.login(username, password)){
					//viene settata una variabile "session" con l'username dell'utente loggato
					session.setAttribute("loggedUser", username);
					getAgentePage(request, response, username);
				}
				else
					getErrorPage(request, response, "Username o password errati!");
			}

			
			/**
			 * Logout
			 */
			else if (ps.equals("logout")) {
				//rimossa la variabile "session" dell'utente
				session.removeAttribute("loggedUser");
				getHomePage(request, response);
			}


			/**
			 * 404
			 */
			else{
				getErrorPage(request, response, "Errore.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}





	/**
	 * Metodo doGet
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
		String ps= "";
		HttpSession session = request.getSession();
		
		if (request.getParameter("ps") != null) {
			ps= request.getParameter("ps");
		}
	
		try {

			/**
			 * HomePage
			 */
			if (ps.equals("")) {
				//nessun parametro: carica la home page
				getHomePage(request, response);
			}
			

			/**
			 * PolizzaPage
			 */
			else if (ps.equals("polizza")) {
				if (request.getParameter("tipo") != null)
					getPolizzaPage(request, response, request.getParameter("tipo"));
				else
					getErrorPage(request, response, "Parametri non corretti.");
			}

			
			/**
			 * AgentePage
			 */
			else if (ps.equals("agente")) {
				if (request.getParameter("matr") != null){
					
					//utente già loggato con la matricola richiesta ora: carica la pagina
					if (session.getAttribute("loggedUser")!= null && session.getAttribute("loggedUser").equals(request.getParameter("matr")))
						getAgentePage(request, response, request.getParameter("matr"));

					//utente già loggato ma richiede una matricola che non è sua: errore di permesso
					else if (session.getAttribute("loggedUser")!= null)
						getErrorPage(request, response, "Non disponi dei permessi necessari per visitare questa pagina.");

					//utente non loggato: reindirizza alla pagina di login
					else
						getLoginPage(request, response, request.getParameter("matr"));
				}
				
				else
					getErrorPage(request, response, "Parametri non corretti.");
			}


			/**
			 * PolizzaGestPage
			 */
			else if (ps.equals("gest")) {
				if (request.getParameter("tipo") != null && request.getParameter("num") != null){

					//recupero dei parametri
					String tipo = request.getParameter("tipo");
					int numero = 0;
					
					try{
						numero = Integer.parseInt(request.getParameter("num"));
					} catch(Exception e){
						getErrorPage(request, response, "Il numero della polizza deve contenere solo cifre.");
					}
					
					//controllo dello stato dell'utente (loggato o no)
					if (session.getAttribute("loggedUser")!= null)
						getPolizzaGestPage(request, response, tipo, numero);
					else
						getErrorPage(request, response, "Non disponi dei permessi necessari per visitare questa pagina.");
				}
				else
					getErrorPage(request, response, "Parametri non corretti.");
			}


			/**
			 * 404
			 */
			else{
				getErrorPage(request, response, "404 - Pagina non trovata.");
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
    }






    
    private void getHomePage(HttpServletRequest request, HttpServletResponse response) throws Exception{
		//caricamento DBMS ed esecuzione query
		DBMS dbms = new DBMS();
		RequestDispatcher rd = null;
		Vector tipipolizza = dbms.getTipoPolizza();

		//viene salvato il risultato della query in un attributo che sarà poi recuperato dalla pagina caricata
		request.setAttribute("tp",tipipolizza);

		//caricamento della pagina richiesta
		rd = request.getRequestDispatcher("../HomePage.jsp");
		rd.forward(request,response);	
	}

	
	private void getPolizzaPage(HttpServletRequest request, HttpServletResponse response, String tipo) throws Exception{
		DBMS dbms = new DBMS();
		RequestDispatcher rd = null;
		Vector polizza = dbms.getDescPolizza(tipo);
		Vector agenti = dbms.getAgentiPolizza(tipo);
		if (polizza.size() > 0){
			request.setAttribute("pol",polizza);
			request.setAttribute("ag",agenti);
			rd = request.getRequestDispatcher("../PolizzaPage.jsp");
			rd.forward(request,response);
		}

		//se il vettore delle polizze è vuoto vuol dire che è il tal tipo di polizza non esiste
		else
			getErrorPage(request, response, "404 - Pagina non trovata.");
	}

	
	private void getAgentePage(HttpServletRequest request, HttpServletResponse response, String matricola) throws Exception{
		DBMS dbms = new DBMS();
		RequestDispatcher rd = null;
		Vector polizzeAutoAgente = dbms.getPolizzeAgente(matricola, "Auto");
		Vector polizzeVitaAgente = dbms.getPolizzeAgente(matricola, "Vita");
		Vector polizzeImmobileAgente = dbms.getPolizzeAgente(matricola, "Immobile");
		Vector agente = dbms.getAgente(matricola);
		request.setAttribute("paa",polizzeAutoAgente);
		request.setAttribute("pva",polizzeVitaAgente);
		request.setAttribute("pia",polizzeImmobileAgente);
		request.setAttribute("ag",agente);
		rd = request.getRequestDispatcher("../AgentePage.jsp");
		rd.forward(request,response);
	}

	
	private void getPolizzaGestPage(HttpServletRequest request, HttpServletResponse response, String tipo, int num) throws Exception{
		DBMS dbms = new DBMS();
		RequestDispatcher rd = null;
		Vector polizza = dbms.getPolizza(tipo, num);
		if (polizza.size() > 0){
			if (tipo.equals("Vita")){
				Vector eredi = dbms.getEredi(tipo, num);
				request.setAttribute("er",eredi);
			}
			request.setAttribute("pol",polizza);
			rd = request.getRequestDispatcher("../PolizzaGestPage.jsp");
			rd.forward(request,response);
		}
		else
			getErrorPage(request, response, "404 - Pagina non trovata.");
	}

	
	private void getLoginPage(HttpServletRequest request, HttpServletResponse response, String username) throws Exception{
		RequestDispatcher rd = null;
		request.setAttribute("username",username);
		rd = request.getRequestDispatcher("../Login.jsp");
		rd.forward(request,response);
	}

	
	private void getErrorPage(HttpServletRequest request, HttpServletResponse response, String messaggio) throws Exception{
		RequestDispatcher rd = null;
		request.setAttribute("msg",messaggio);
		rd = request.getRequestDispatcher("../Error.jsp");
		rd.forward(request,response);
	}
	
}
