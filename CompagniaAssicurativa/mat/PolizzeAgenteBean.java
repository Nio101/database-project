package mat;

public class PolizzeAgenteBean {
	private String tipo;
	private int numero;
	private String stato;
	private String cognomeCliente;
	private String dinizio;
	private String dfine;

	
    public PolizzeAgenteBean() {
		tipo = null;
		stato = null;
		cognomeCliente = null;
		dinizio = null;
		dfine = null;
    }

    //metodi set
	public void setTipo(String x) {
		tipo = x;
    }

    public void setNumero(int x) {
		numero = x;
    }

    public void setStato(String x) {
		stato = x;
    }

    public void setCognomeCliente(String x) {
		cognomeCliente = x;
    }

    public void setDinizio(String x) {
		dinizio = x;
    }

	public void setDfine(String x) {
		dfine = x;
    }
    
    //metodi get
	public String getTipo() {
		return tipo;
	}

	public int getNumero() {
		return numero;
	}

	public String getStato() {
		return stato;
	}

	public String getCognomeCliente() {
		return cognomeCliente;
	}
	
	public String getDinizio() {
		return dinizio;
	}

	public String getDfine() {
		return dfine;
	}
	
}
