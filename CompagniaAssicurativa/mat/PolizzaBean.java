package mat;

public class PolizzaBean {
	private int numero;
	private String tipo;
	private String stato;
	private String matricolaAgente;
	private String nomeAgente;
	private String cognomeAgente;
	private String nomeTitolare;
	private String cognomeTitolare;
	private int importo;
	private String frequenza_pagamento;
	private String dataInizio;
	private String dataFine;
	private String targa;
	private int massimale;
	private String furto_incendio;
	private String indirizzo;
	private int superficie;
								
	
    public PolizzaBean() {
		tipo = null;
		stato = null;
		matricolaAgente = null;
		nomeAgente = null;
		cognomeAgente = null;
		nomeTitolare = null;
		cognomeTitolare = null;
		frequenza_pagamento = null;
		dataInizio = null;
		dataFine = null;
		targa = null;
		furto_incendio = null;
		indirizzo = null;
    }

    //metodi set

	public void setNumero(int x){
		numero = x;
	}
	
	public void setTipo(String x){
		tipo = x;
	}
	
	public void setStato(String x){
		stato = x;
	}
	
	public void setMatricolaAgente(String x){
		matricolaAgente = x;
	}

	public void setNomeAgente(String x){
		nomeAgente = x;
	}
	
	public void setCognomeAgente(String x){
		cognomeAgente = x;
	}
	
	public void setNomeTitolare(String x){
		nomeTitolare = x;
	}
	
	public void setCognomeTitolare(String x){
		cognomeTitolare = x;
	}
	
	public void setImporto(int x){
		importo = x;
	}
	
	public void setFrequenza(String x){
		frequenza_pagamento = x;
	}
	
	public void setDataInizio(String x){
		dataInizio = x;
	}
	
	public void setDataFine(String x){
		dataFine = x;
	}
	
	public void setTarga(String x){
		targa = x;
	}
	
	public void setMassimale(int x){
		massimale = x;
	}
	
	public void setFurtoIncendio(String x){
		furto_incendio = x;
	}
	
	public void setIndirizzo(String x){
		indirizzo = x;
	}
	
	public void setSuperficie(int x){
		superficie = x;
	}
    

    
    //metodi get

	public int getNumero(){
		return numero;
	}

	public String getTipo(){
		return tipo;
	}

	public String getStato(){
		return stato;
	}

	public String getMatricolaAgente(){
		return matricolaAgente;
	}

	public String getNomeAgente(){
		return nomeAgente;
	}

	public String getCognomeAgente(){
		return cognomeAgente;
	}

	public String getNomeTitolare(){
		return nomeTitolare;
	}

	public String getCognomeTitolare(){
		return cognomeTitolare;
	}

	public int getImporto(){
		return importo;
	}

	public String getFrequenza(){
		return frequenza_pagamento;
	}

	public String getDataInizio(){
		return dataInizio;
	}

	public String getDataFine(){
		return dataFine;
	}

	public String getTarga(){
		return targa;
	}

	public int getMassimale(){
		return massimale;
	}

	public String getFurtoIncendio(){
		return furto_incendio;
	}

	public String getIndirizzo(){
		return indirizzo;
	}

	public int getSuperficie(){
		return superficie;
	}	
}
