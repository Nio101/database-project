package mat;

import java.sql.*;
import java.util.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class DBMS {
    private String user = "userlab49";
    private String passwd = "quarantanovevA";
    private String url = "jdbc:postgresql://dbserver.scienze.univr.it/dblab49";
    private String driver = "org.postgresql.Driver";

	private String qDesc = "SELECT * FROM TipoPolizza";
	private String qSpecDesc = "SELECT * FROM TipoPolizza WHERE TipoPolizza.tipo = ?";
	private String qAgenti = "SELECT DISTINCT A.matricola, A.nome, A.cognome, A.area, A.email FROM Agente A join Polizza P on A.matricola = P.matricola_agente WHERE P.tipo = ?";
	private String qAgente = "SELECT matricola, nome, cognome, area, email FROM Agente WHERE matricola = ?";
	private String qPolizzeAgente = "SELECT P.tipo, P.numero, P.stato, C.cognome, P.dinizio, P.dfine	FROM (Polizza P join Cliente C on P.uid = C.uid) join Agente A on P.matricola_agente = A.matricola WHERE A.matricola = ? AND P.tipo = ?";
	private String qPolGen = "SELECT P.numero, P.tipo, P.stato, A.matricola, A.nome AS nomeAgente, A.Cognome AS cognomeAgente, C.nome AS nomeCliente, C.cognome AS cognomeCliente, P.importo, P.frequenza_pagamento, P.dinizio, P.dfine FROM (Polizza P join  Agente A on P.matricola_agente = A.matricola) join Cliente C on P.uid = C.uid WHERE P.tipo = ? AND P.numero = ?";
	private String qPolAuto = "SELECT PA.targa, PA.massimale, PA.furto_incendio FROM PolizzaAuto PA WHERE PA.tipo = ? AND PA.numero = ?";
	private String qPolImm = "SELECT PI.indirizzo, PI.superficie FROM PolizzaImmobile PI WHERE PI.tipo = ? AND PI.numero = ?";
	private String qPolEredi = "SELECT DISTINCT E.CF, E.nome, E.cognome, E.indirizzo_residenza, E.citta_residenza 	FROM (PolizzaVita PV join Beneficiario B on (PV.tipo = B.tipo AND PV.numero = B.numero)) join Erede E on B.CFerede = E.CF WHERE PV.tipo = ? AND PV.numero = ?";
	private String qLogin = "SELECT matricola, password FROM Agente WHERE matricola = ? AND password = ?";

    public DBMS() throws ClassNotFoundException {
		Class.forName(driver);
    }


	public boolean login(String username, String password) throws SQLException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qLogin);
			pstmt.clearParameters();
			pstmt.setString(1, username);
			pstmt.setString(2, hashPassword(password));
			rs=pstmt.executeQuery();

			if (rs.next()){
				return true;
			}
			return false;
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return false;
	}


	public static String hashPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();

            for (byte b : digest)
                sb.append(String.format("%02x", b));

            return sb.toString();
        } catch (NoSuchAlgorithmException nsae) {
            System.out.println("errore algoritmo hashing: " + nsae.getMessage());
            nsae.printStackTrace();
        } catch (UnsupportedEncodingException uee) {
            System.out.println("errore codifica: " + uee.getMessage());
            uee.printStackTrace();
        }

        return null;
    }


    
    
	private TipoPolizzaBean makeTipoPolizzaBean(ResultSet rs) throws SQLException {
		TipoPolizzaBean bean = new TipoPolizzaBean();
		bean.setTipo(rs.getString("tipo"));
		bean.setDescrizione(rs.getString("descrizione"));
		
		return bean;
    }

	public Vector getTipoPolizza() {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			stmt = con.createStatement();
			rs = stmt.executeQuery(qDesc);
			while(rs.next())
				result.add(makeTipoPolizzaBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

	public Vector getDescPolizza(String tipo) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qSpecDesc);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			rs=pstmt.executeQuery();
			
			while(rs.next())
				result.add(makeTipoPolizzaBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

	/**************************/

	private AgenteBean makeAgenteBean(ResultSet rs) throws SQLException {
		AgenteBean bean = new AgenteBean();
		bean.setMatricola(rs.getString("matricola"));
		bean.setNome(rs.getString("nome"));
		bean.setCognome(rs.getString("cognome"));
		bean.setArea(rs.getString("area"));
		bean.setEmail(rs.getString("email"));

		return bean;
    }

    public Vector getAgentiPolizza(String tipo) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qAgenti);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			rs=pstmt.executeQuery();

			while(rs.next())
				result.add(makeAgenteBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

    public Vector getAgente(String matricola) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qAgente);
			pstmt.clearParameters();
			pstmt.setString(1, matricola);
			rs=pstmt.executeQuery();

			while(rs.next())
				result.add(makeAgenteBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

    /**************************/

	private PolizzeAgenteBean makePolizzeAgenteBean(ResultSet rs) throws SQLException {
		PolizzeAgenteBean bean = new PolizzeAgenteBean();
		bean.setTipo(rs.getString("tipo"));
		bean.setNumero(rs.getInt("numero"));
		bean.setStato(rs.getString("stato"));
		bean.setCognomeCliente(rs.getString("cognome"));
		bean.setDinizio(rs.getString("dinizio"));
		bean.setDfine(rs.getString("dfine"));

		return bean;
    }

    public Vector getPolizzeAgente(String matricola, String tipo) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qPolizzeAgente);
			pstmt.clearParameters();
			pstmt.setString(1, matricola);
			pstmt.setString(2, tipo);
			rs=pstmt.executeQuery();

			while(rs.next())
				result.add(makePolizzeAgenteBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

	/**************************/

	private PolizzaBean makePolizzaBean(ResultSet rs1, ResultSet rs2, ResultSet rs3, boolean auto, boolean immobile) throws SQLException {
		PolizzaBean bean = new PolizzaBean();
		bean.setNumero(rs1.getInt("numero"));
		bean.setTipo(rs1.getString("tipo"));
		bean.setStato(rs1.getString("stato"));
		bean.setMatricolaAgente(rs1.getString("matricola"));
		bean.setNomeAgente(rs1.getString("nomeAgente"));
		bean.setCognomeAgente(rs1.getString("cognomeAgente"));
		bean.setNomeTitolare(rs1.getString("nomeCliente"));
		bean.setCognomeTitolare(rs1.getString("cognomeCliente"));
		bean.setImporto(rs1.getInt("importo"));
		bean.setFrequenza(rs1.getString("frequenza_pagamento"));
		bean.setDataInizio(rs1.getString("dinizio"));
		bean.setDataFine(rs1.getString("dfine"));
		if (auto){
			bean.setTarga(rs2.getString("targa"));
			bean.setMassimale(rs2.getInt("massimale"));
			bean.setFurtoIncendio(rs2.getString("furto_incendio"));
		}
		if (immobile){
			bean.setIndirizzo(rs3.getString("indirizzo"));
			bean.setSuperficie(rs3.getInt("superficie"));
		}
		
		return bean;
    }

    public Vector getPolizza(String tipo, int numero) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		ResultSet rs3 = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qPolGen);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			pstmt.setInt(2, numero);
			rs1=pstmt.executeQuery();

			pstmt = con.prepareStatement(qPolAuto);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			pstmt.setInt(2, numero);
			rs2=pstmt.executeQuery();

			pstmt = con.prepareStatement(qPolImm);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			pstmt.setInt(2, numero);
			rs3=pstmt.executeQuery();

			rs1.next();
			boolean auto = rs2.next();
			boolean immobile = rs3.next();
			result.add(makePolizzaBean(rs1, rs2, rs3, auto, immobile));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }

    /**************************/

	private EredeBean makeEredeBean(ResultSet rs) throws SQLException {
		EredeBean bean = new EredeBean();
		bean.setCF(rs.getString("CF"));
		bean.setNome(rs.getString("nome"));
		bean.setCognome(rs.getString("cognome"));
		bean.setIndirizzo(rs.getString("indirizzo_residenza"));
		bean.setCitta(rs.getString("citta_residenza"));

		return bean;
    }

    public Vector getEredi(String tipo, int numero) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(qPolEredi);
			pstmt.clearParameters();
			pstmt.setString(1, tipo);
			pstmt.setInt(2, numero);
			rs=pstmt.executeQuery();

			while(rs.next())
				result.add(makeEredeBean(rs));
		} catch(SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
    }
}
