package mat;

public class AgenteBean {
	private String matricola;
	private String nome;
	private String cognome;
	private String area;
	private String email;

	
    public AgenteBean() {
		matricola = null;
		nome = null;
		cognome = null;
		area = null;
		email = null;
    }

    //metodi set
	public void setMatricola(String x) {
		matricola = x;
    }

    public void setNome(String x) {
		nome = x;
    }

    public void setCognome(String x) {
		cognome = x;
    }

    public void setArea(String x) {
		area = x;
    }

    public void setEmail(String x) {
		email = x;
    }
	
    //metodi get
	public String getMatricola() {
		return matricola;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getArea() {
		return area;
	}
	
	public String getEmail() {
		return email;
	}
	
}
