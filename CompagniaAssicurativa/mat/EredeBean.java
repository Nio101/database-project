package mat;

public class EredeBean {
	private String CF;
	private String nome;
	private String cognome;
	private String indirizzo_residenza;
	private String citta_residenza;


	public EredeBean() {
		CF = null;
		nome = null;
		cognome = null;
		indirizzo_residenza = null;
		citta_residenza = null;
    }

    //metodi set
	public void setCF(String x) {
		CF = x;
    }

    public void setNome(String x) {
		nome = x;
    }

    public void setCognome(String x) {
		cognome = x;
    }

    public void setIndirizzo(String x) {
		indirizzo_residenza = x;
    }

    public void setCitta(String x) {
		citta_residenza = x;
    }
	
    //metodi get
	public String getCF() {
		return CF;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getIndirizzo() {
		return indirizzo_residenza;
	}
	
	public String getCitta() {
		return citta_residenza;
	}
	
}
