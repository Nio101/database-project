package mat;

public class TipoPolizzaBean {
	private String tipo;
	private String descrizione;

    public TipoPolizzaBean() {
		tipo = null;
		descrizione = null;
    }

    //metodi set
    public void setTipo(String x) {
		tipo = x;
    }

    public void setDescrizione(String x) {
		descrizione = x;
    }
	
    //metodi get
	public String getTipo() {
		return tipo;
	}

	public String getDescrizione() {
		return descrizione;
	}
}
