<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="mat.*"%>
<%@ page errorPage="Error.jsp" %>


<html>
	<%
		Vector polizza = (Vector)request.getAttribute("pol");
		Vector eredi = (Vector)request.getAttribute("er");
		PolizzaBean pb;
		EredeBean eb;
	%>
	
<head>
	<link rel="stylesheet" media="screen" href="../main.css")">
	<title>Assicuratori Matera</title>
</head>

<body>
	<div class="topbar">
		<div class="toptitle">
			<a href="main" >Assicuratori Matera</a> Compagnia assicurativa dal 1987.
		</div>
	</div>

	<div class="titlebar">
		<%	pb = (PolizzaBean)polizza.get(0); %>
		<h1>Polizza <%= pb.getTipo() %> numero <%= pb.getNumero() %></h1>
		<div class="logout">
			<%	if (session.getAttribute("loggedUser")!=null){ %>
						<br><br>
						<form name="logout" action="main?ps=logout" method="post">
							<input type=submit name="logout" value="Logout">
						</form>
			<%	} %>
		</div>
	</div>


	<div class="content">	
		<table>
			<tr><th>Attributo</th><th>Valore</th></tr>
			<tr><td>Tipo</td><td><%= pb.getTipo() %></td></tr>
			<tr><td>Numero</td><td><%= pb.getNumero() %></td></tr>
			<tr><td>Stato</td><td><%= pb.getStato() %></td></tr>
			<tr><td>Nome Agente</td><td><%= pb.getNomeAgente() %></td></tr>
			<tr><td>Cognome Agente</td><td><%= pb.getCognomeAgente() %></td></tr>
			<tr><td>Nome Cliente</td><td><%= pb.getNomeTitolare() %></td></tr>
			<tr><td>Cognome Cliente</td><td><%= pb.getCognomeTitolare() %></td></tr>
			<tr><td>Importo</td><td><%= pb.getImporto() %></td></tr>
			<tr><td>Frequenza Pagamento</td><td><%= pb.getFrequenza() %></td></tr>
			<tr><td>Data di inizio</td><td><%= pb.getDataInizio() %></td></tr>
			<tr><td>Data di fine</td><td><%= pb.getDataFine() %></td></tr>

			<%	if (pb.getTarga()!= null) {		%>
						<tr><td>Targa</td><td><%= pb.getTarga() %></td></tr>
						<tr><td>Massimale</td><td><%= pb.getMassimale() %></td></tr>
						<tr><td>Furto Incendio</td><td><%= pb.getFurtoIncendio() %></td></tr>
			<%	}	%>

			<%	if (pb.getIndirizzo()!= null) {		%>
						<tr><td>Indirizzo immobile</td><td><%= pb.getIndirizzo() %></td></tr>
						<tr><td>Superficie (mq)</td><td><%= pb.getSuperficie() %></td></tr>
			<%	}	%>
		</table>


		<%	if (eredi != null){		%>
			<br><br>
			<table><caption>Eredi</caption>
				<tr><th>Codice Fiscale</th><th>Nome</th><th>Cognome</th><th>Indirizzo di residenza</th><th>Citt&agrave di residenza</th></tr>
				<%	for (int i=0; i < eredi.size(); i++) {
							eb = (EredeBean)eredi.get(i);		%>
							<tr>
							<td><%= eb.getCF() %></td>
							<td><%= eb.getNome() %></td>
							<td><%= eb.getCognome() %></td>
							<td><%= eb.getIndirizzo() %></td>
							<td><%= eb.getCitta() %></td>
							</tr>
				<%	}	%>
			</table>
		<%	}	%>
	</div>

	<div class="footer">
		Assicurazioni Matera - via Garibaldi 10, 25087 Sal&ograve;, Brescia <br>
		email: info@assicurazionimatera.it<br>
		tel: 0365/123456
	</div>
</body>
</html>
