<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="mat.*"%>
<%@ page errorPage="Error.jsp" %>


<html>

	<%
		Vector polizzeAutoAgente = (Vector)request.getAttribute("paa");
		Vector polizzeVitaAgente = (Vector)request.getAttribute("pva");
		Vector polizzeImmobileAgente = (Vector)request.getAttribute("pia");
		Vector agente = (Vector)request.getAttribute("ag");
		AgenteBean ab;
		PolizzeAgenteBean pab;
	%>
	
<head>
	<link rel="stylesheet" media="screen" href="../main.css")">
	<title>Assicuratori Matera</title>
</head>

<body>
	<div class="topbar">
		<div class="toptitle">
			<a href="main" >Assicuratori Matera</a> Compagnia assicurativa dal 1987.
		</div>
	</div>

	<div class="titlebar">
		<%	ab = (AgenteBean)agente.get(0);		%>
		<h1><%= ab.getNome() %> <%= ab.getCognome() %></h1>
		<div class="logout">
			<%	if (session.getAttribute("loggedUser")!=null){ %>
						<br><br>
						<form name="logout" action="main?ps=logout" method="post">
							<input type=submit name="logout" value="Logout">
						</form>
			<%	} %>
		</div>
	</div>


	<div class="content">
		<%	if (polizzeAutoAgente.size()>0){		%>
					<br><br>
					<table><caption>Polizze Auto</caption>
						<tr><th>Numero polizza</th><th>Stato</th><th>Cognome del cliente</th><th>Data di inizio</th><th>Data di fine</th></tr>
						<%	for (int i=0; i < polizzeAutoAgente.size(); i++) {
									pab = (PolizzeAgenteBean)polizzeAutoAgente.get(i);	%>
									<tr>
									<td><a href="main?ps=gest&tipo=<%= pab.getTipo() %>&num=<%= pab.getNumero() %>"><%= pab.getNumero() %></a></td>
									<td><%= pab.getStato() %></td>
									<td><%= pab.getCognomeCliente() %></td>
									<td><%= pab.getDinizio() %></td>
									<td><%= pab.getDfine() %></td>
									</tr>
						<%	}	%>
					</table>
		<%	}	%>



		<%	if (polizzeVitaAgente.size()>0){	%>
					<br><br>
					<table><caption>Polizze Vita</caption>
						<tr><th>Numero polizza</th><th>Stato</th><th>Cognome del cliente</th><th>Data di inizio</th><th>Data di fine</th></tr>
						<%	for (int i=0; i < polizzeVitaAgente.size(); i++) {
									pab = (PolizzeAgenteBean)polizzeVitaAgente.get(i);	%>
									<tr>
									<td><a href="main?ps=gest&tipo=<%= pab.getTipo() %>&num=<%= pab.getNumero() %>"><%= pab.getNumero() %></a></td>
									<td><%= pab.getStato() %></td>
									<td><%= pab.getCognomeCliente() %></td>
									<td><%= pab.getDinizio() %></td>
									<td><%= pab.getDfine() %></td>
									</tr>
						<%	}	%>
					</table>
		<%	}	%>



		<%	if (polizzeImmobileAgente.size()>0){	%>
					<br><br>
					<table><caption>Polizze su Immobili</caption>
						<tr><th>Numero polizza</th><th>Stato</th><th>Cognome del cliente</th><th>Data di inizio</th><th>Data di fine</th></tr>
						<%	for (int i=0; i < polizzeImmobileAgente.size(); i++) {
									pab = (PolizzeAgenteBean)polizzeImmobileAgente.get(i);		%>
									<tr>
									<td><a href="main?ps=gest&tipo=<%= pab.getTipo() %>&num=<%= pab.getNumero() %>"><%= pab.getNumero() %></a></td>
									<td><%= pab.getStato() %></td>
									<td><%= pab.getCognomeCliente() %></td>
									<td><%= pab.getDinizio() %></td>
									<td><%= pab.getDfine() %></td>
									</tr>
						<%	}	%>
					</table>
		<%	}	%>
	</div>

	<div class="footer">
		Assicurazioni Matera - via Garibaldi 10, 25087 Sal&ograve;, Brescia <br>
		email: info@assicurazionimatera.it<br>
		tel: 0365/123456
	</div>
</body>
</html>
