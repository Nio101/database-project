<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="mat.*"%>
<%@ page isErrorPage="true" %>

<html>
	<%
		String msg = (String)request.getAttribute("msg");
	%>

	<head>
		<link rel="stylesheet" media="screen" href="../main.css")">
		<title>Assicuratori Matera</title>
	</head>

	<body>
		<div class="topbar">
			<div class="toptitle">
				<a href="main" >Assicuratori Matera</a> Compagnia assicurativa dal 1987.
			</div>
		</div>

		<div class="titlebar">
			<div class="error">
				<h1>Errore</h1>
				<%	if (msg != null){ %>
							<%=msg%>
				<%	}	%>
			</div>
			<%	if (msg == null){ %>
						<pre> <%= exception %> </pre>
						<pre> <% exception.printStackTrace(new java.io.PrintWriter(out)); %> </pre>
			<%	}	%>
		</div>		
	</body>
</html>
