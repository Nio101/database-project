<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="mat.*"%>
<%@ page errorPage="Error.jsp" %>

<html>
	<%
		Vector descpolizza = (Vector)request.getAttribute("pol");
		Vector agenti = (Vector)request.getAttribute("ag");
		TipoPolizzaBean tpb;
		AgenteBean ab;
	%>
	
<head>
	<link rel="stylesheet" media="screen" href="../main.css")">
	<title>Assicuratori Matera</title>
</head>

<body>
	<div class="topbar">
		<div class="toptitle">
			<a href="main" >Assicuratori Matera</a> Compagnia assicurativa dal 1987.
		</div>
	</div>

	<div class="titlebar">
		<% tpb = (TipoPolizzaBean)descpolizza.get(0); %>
		<h1>Polizza <%= tpb.getTipo() %></h1>
		<div class="logout">
			<%	if (session.getAttribute("loggedUser")!=null){ %>
						<br><br>
						<form name="logout" action="main?ps=logout" method="post">
							<input type=submit name="logout" value="Logout">
						</form>
			<%	} %>
		</div>
	</div>

	<div class="content">
		
		<%= tpb.getDescrizione() %>
		<br><br><br>

		Ecco gli agenti che si occupano di questo tipo di polizza:

		<br><br>

		<table>
			<tr><th>Nome</th><th>Cognome</th><th>Area di competenza</th><th>Email</th><th></th></tr>
			<%	for (int i=0; i < agenti.size(); i++) {
						ab = (AgenteBean)agenti.get(i);  %>
						<tr>
						<td><%= ab.getNome() %></a></td>
						<td><%= ab.getCognome() %></td>
						<td><%= ab.getArea() %></td>
						<td><%= ab.getEmail() %></td>
						<td><a href="main?ps=agente&matr=<%= ab.getMatricola() %>">Area riservata agente</a></td>
						</tr>
			<%	}	%>
		</table>
	</div>

	<div class="footer">
		Assicurazioni Matera - via Garibaldi 10, 25087 Sal&ograve;, Brescia <br>
		email: info@assicurazionimatera.it<br>
		tel: 0365/123456
	</div>
</body>
</html>
