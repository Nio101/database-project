<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="mat.*"%>
<%@ page errorPage="Error.jsp" %>


<html>
	<%
		Vector tipipolizza = (Vector)request.getAttribute("tp");
		TipoPolizzaBean tpb;
	%>

<head>
	<link rel="stylesheet" media="screen" href="../main.css")">
	<title>Assicuratori Matera</title>
</head>

<body>
	<div class="topbar">
		<div class="toptitle">
			<a href="main" >Assicuratori Matera</a> Compagnia assicurativa dal 1987.
		</div>
	</div>

	<div class="titlebar">
		<h1>Home Page</h1>
		<div class="loginform">
			<%	if (session.getAttribute("loggedUser")==null){ %>
						<form name="login" action="main?ps=login" method="post">
							<input name="username"  type="text" value="Username">
							<input name="password"  type="password" value="Password"><br>
							<input type=submit name="submit" value="Login">
							<%-- <input type=reset name="reset" value="Annulla"> --%>
						</form>
			<%	} %>
		</div>
		<div class="logout">
			<%	if (session.getAttribute("loggedUser")!=null){ %>
						<br><br>
						<form name="logout" action="main?ps=logout" method="post">
							<input type=submit name="logout" value="Logout">
						</form>
			<%	} %>
		</div>
	</div>
	
	<div class="content">
	
	<%	if (tipipolizza!=null){ %>
				<br><br>
				<table border="1">
					<caption>Polizze disponibili</caption>
					<tr><th>Tipo</th><th>Descrizione</th></tr>
					<%	for (int i=0; i < tipipolizza.size(); i++) {
								tpb = (TipoPolizzaBean)tipipolizza.get(i);  %>
								<tr>
								<td width=100px><a href="main?ps=polizza&tipo=<%= tpb.getTipo() %>"><%= tpb.getTipo() %></a></td>
								<td><%= tpb.getDescrizione() %></td>
								</tr>
					<%	} %>
				</table>
	<%	} %>
	

	<br><br><br><br>
	A titolo di esempio i dati di login sono:
	<ul>
		<li>AG08145 / truffaldino72</li>
		<li>AG09876 / pischell0</li>
		<li>AG09536 / s0rtilegi0</li>
		<li>AG11271 / schiaffato98</li>
	</ul>

	<%	if (session.getAttribute("loggedUser")!=null){ %>
				<br><br>
				<form name="logout" action="main?ps=logout" method="post">
					<input type=submit name="logout" value="Logout">
				</form>
	<%	} %>
	
	</div>

	<div class="footer">
		Assicurazioni Matera - via Garibaldi 10, 25087 Sal&ograve;, Brescia <br>
		email: info@assicurazionimatera.it<br>
		tel: 0365/123456
	</div>
</body>
</html>
